package cz.cvut.fel.ts1.cv1;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

public class FactorialCalculatorTest {

    @Test
    public void factorialTest() {
        FactorialCalculator factorialCalculator = new FactorialCalculator();
        int number = 3;
        long expectedResult = 6;

        long result = factorialCalculator.factorial(number);

        Assertions.assertEquals(expectedResult, result);
    }
}