package cz.cvut.fel.ts1.cv4.shop;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @ParameterizedTest(name = "Test with items: {0}, customer: {1}, address: {2}, state: {3}")
    @CsvSource({
            "101, 'Orange', 3.99, 'Fruits', 8, 'John Smith', 'New York', 1",
            "202, 'Banana', 2.49, 'Fruits', 5, 'Emily Johnson', 'Los Angeles', 2"
    })
    public void testConstructorWithItems(int itemId1, String itemName1, float itemPrice1,
                                         String itemCategory1, int itemLoyaltyPoints1,
                                         String customerName, String customerAddress, int state) {
        // Create items list
        ArrayList<Item> items = new ArrayList<>();
        items.add(new StandardItem(itemId1, itemName1, itemPrice1, itemCategory1, itemLoyaltyPoints1));

        // Create shopping cart with items
        ShoppingCart shoppingCart = new ShoppingCart(items);

        // Create order
        Order order = new Order(shoppingCart, customerName, customerAddress, state);
        ArrayList<Item> resultItems = order.getItems();
        String resultName = order.getCustomerName();
        String resultAddress = order.getCustomerAddress();
        int resultState = order.getState();
        Assertions.assertEquals(shoppingCart.getCartItems(), resultItems);
        Assertions.assertEquals(customerName, resultName);
        Assertions.assertEquals(customerAddress, resultAddress);
        Assertions.assertEquals(state, resultState);
    }


    @ParameterizedTest(name = "Test without items: {0}, customer: {1}, address: {2}")
    @CsvSource({
            "'Sophia Brown', 'Chicago'",
            "'William Johnson', 'Houston'"
    })
    public void testConstructorWithoutItems(String customerName, String customerAddress) {
        // Create shopping cart without items
        ShoppingCart shoppingCart = new ShoppingCart(null);

        // Create order
        Order order = new Order(shoppingCart, customerName, customerAddress);
        ArrayList<Item> resultItems = order.getItems();
        String resultName = order.getCustomerName();
        String resultAddress = order.getCustomerAddress();
        int resultState = order.getState();
        Assertions.assertEquals(shoppingCart.getCartItems(), resultItems);
        Assertions.assertEquals(customerName, resultName);
        Assertions.assertEquals(customerAddress, resultAddress);
        Assertions.assertEquals(0, resultState);
    }


    @Test
    @DisplayName("Test constructor with null arguments")
    public void testConstructorWithNullArgument() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            ShoppingCart cart = null;
            String customerName = null;
            String customerAddress = null;
            new Order(cart, customerName, customerAddress);
        });
    }
}