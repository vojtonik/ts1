package cz.cvut.fel.ts1.cv4.storage;

import cz.cvut.fel.ts1.cv4.shop.Item;
import cz.cvut.fel.ts1.cv4.shop.StandardItem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.MockedConstruction;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ItemStockTest {
    @ParameterizedTest
    @CsvSource({ "10, 10", "20, 20", "30, 30" })
    public void increaseItemCountTest(int addItems, int expectedResult) {
        Item item = new StandardItem(22, "Apple", 1.23f, "Fruits", 77);
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(addItems);
        Assertions.assertEquals(expectedResult, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({ "10, 5, 5", "20, 10, 10", "30, 20, 10" })
    public void testDecreaseItemCount(int initialItemCount, int removedItemCount, int expectedRemainingItemCount) {
        Item item = new StandardItem(22, "Apple", 1.50f, "Fruits", 80); // Different fruit: Apple
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(initialItemCount);
        itemStock.decreaseItemCount(removedItemCount);
        Assertions.assertEquals(expectedRemainingItemCount, itemStock.getCount());
    }
}