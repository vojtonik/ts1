package cz.cvut.fel.ts1.cv4.shop;

import cz.cvut.fel.ts1.cv4.storage.NoItemInStorage;
import cz.cvut.fel.ts1.cv4.storage.Storage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class EShopControllerTest {

    private EShopController eShopController;
    private Storage storage;
    private Item[] storageItems;
    private int[] itemCount;
    private ShoppingCart cart;
    private Order order;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();


    @BeforeEach
    void setUp() {

        eShopController = new EShopController();
        storage = new Storage();
        storageItems = new Item[]{
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };
        itemCount = new int[]{10, 10, 4, 5, 10, 2};
        int i = 0;
        while (i < storageItems.length) {
            storage.insertItems(storageItems[i], itemCount[i]);
            i++;
        }
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }


    @Test
    public void purchaseShoppingCartEmptyTest() throws NoItemInStorage {
        String customerName = "Libuse Novakova";
        String customerAddress = "Kosmonautu 25, Praha 8";
        String expectedOutput = "Error: shopping cart is empty";
        cart = new ShoppingCart();
        EShopController.startEShop();
        EShopController.purchaseShoppingCart(cart, customerName, customerAddress);
        assertEquals(expectedOutput, outContent.toString().trim());

    }

}
