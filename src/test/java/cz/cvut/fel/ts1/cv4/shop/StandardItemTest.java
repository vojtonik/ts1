package cz.cvut.fel.ts1.cv4.shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {

    @ParameterizedTest
    @CsvSource({
            "1, testovaciPolozka, 10.0f, testovaciKategorie, 10",
            "2, testovaciPolozka2, 20.0f, testovaciKategorie2, 20"
    })
    public void testKonstruktor(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        Assertions.assertEquals(id, standardItem.getID());
        Assertions.assertEquals(name, standardItem.getName());
        Assertions.assertEquals(price, standardItem.getPrice());
        Assertions.assertEquals(category, standardItem.getCategory());
        Assertions.assertEquals(loyaltyPoints, standardItem.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource({
            "1, testovaciPolozka, 10.0f, testovaciKategorie, 10",
            "2, testovaciPolozka2, 20.0f, testovaciKategorie2, 20"
    })
    public void testKopie(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem kopie = item.copy();
        Assertions.assertEquals(item, kopie);
        Assertions.assertNotSame(item, kopie);
    }


    @ParameterizedTest()
    @CsvSource({
            "1, testovaciPolozka, 10.0f, testovaciKategorie, 10, true",
            "2, testovaciPolozka, 10.0f, testovaciKategorie, 10, false",
            "1, jineJmeno, 10.0f, testovaciKategorie, 10, false",
            "1, testovaciPolozka, 17.0f, testovaciKategorie, 5, false",
            "1, testovaciPolozka, 10.0f, jinaKategorie, 10, false",
            "1, testovaciPolozka, 10.0f, testovaciKategorie, 17, false"
    })
    public void testEquals(int id, String name, float price, String category, int loyaltyPoints, boolean expected) {
        StandardItem item = new StandardItem(1, "testovaciPolozka", 10.0f, "testovaciKategorie", 10);
        StandardItem otherItem = new StandardItem(id, name, price, category, loyaltyPoints);
        assertEquals(expected, item.equals(otherItem));
    }
}