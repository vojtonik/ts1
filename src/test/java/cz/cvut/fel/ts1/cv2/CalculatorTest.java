package cz.cvut.fel.ts1.cv2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private static final String ADD_MESSAGE = "Adding 2 and 3 should equal 5";
    private static final String SUBTRACT_MESSAGE = "Subtracting 2 from 3 should equal 1";
    private static final String MULTIPLY_MESSAGE = "Multiplying 2 and 3 should equal 6";
    private static final String DIVIDE_MESSAGE = "Dividing 6 by 3 should equal 2";
    private static final String DIVIDE_BY_ZERO_MESSAGE = "Attempting to divide by zero should throw DivisionByZeroException";

    @Test
    void add() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(5, calculator.add(2, 3), ADD_MESSAGE);
    }

    @Test
    void subtract() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(1, calculator.subtract(3, 2), SUBTRACT_MESSAGE);
    }

    @Test
    void multiply() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(6, calculator.multiply(2, 3), MULTIPLY_MESSAGE);
    }

    @Test
    void divide() throws DivisionByZeroException {
        // just testing
        Calculator calculator = new Calculator();
        Assertions.assertEquals(2, calculator.divide(6, 3), DIVIDE_MESSAGE);
    }

    @Test
    void divideByZero() {
        Calculator calculator = new Calculator();
        Assertions.assertThrows(DivisionByZeroException.class, () -> calculator.divide(1, 0),
                DIVIDE_BY_ZERO_MESSAGE);
    }
}